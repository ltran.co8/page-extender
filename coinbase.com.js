async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

const getTransaction = async (walletID) => {
  return fetch(
    `https://www.coinbase.com/api/v2/accounts/${walletID}/transactions?expand=all&`
  )
    .then((response) => response.json())
    .then((response) => {
      const { data: transactions } = response;
      const totalCosts = {};

      console.log(transactions);

      transactions.forEach((transaction) => {
        const { native_amount, amount, type } = transaction;
        const { amount: dollars } = native_amount;
        const { amount: tokens, currency } = amount;
        const key = `${currency}_${type}`;

        if (totalCosts[key]) {
          totalCosts[key] = {
            tokens: totalCosts[key].tokens + parseFloat(tokens),
            cost: totalCosts[key].cost + parseFloat(dollars),
          };
        } else {
          totalCosts[key] = {
            tokens: parseFloat(tokens),
            cost: parseFloat(dollars),
          };
        }
      });

      return totalCosts;
    });
};

const createTable = async () => {
  const eth = await getTransaction("2e5afc48-86ee-5efc-869c-80d107ddbe40");
  const btc = await getTransaction("d9c8917c-d97e-5385-9c72-e3deb8e02041");

  const display = document.createElement("textarea");
  display.setAttribute("id", "page-extender-text-area-report");
  display.value = JSON.stringify(
    {
      ...eth,
      ...btc,
    },
    null,
    2
  );
  document.body.appendChild(display);
};

loadScript(
  "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
).then(() => {
  const extenderFloatingBtn = document.createElement("button");
  extenderFloatingBtn.setAttribute("id", "page-extender-floating-button");
  extenderFloatingBtn.innerHTML = "🚀";
  extenderFloatingBtn.addEventListener("click", async () => {
    setTimeout(() => {
      createTable();
    }, 1000);
  });

  document.body.appendChild(extenderFloatingBtn);
});
