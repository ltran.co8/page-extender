async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

const getCookie = (name) => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
};

const createCSV = (rows) => {
  const content = [
    "id,date,description,owner,asset,amount,note,category",
    ...rows,
  ];
  const link = document.createElement("a");
  link.id = "download-csv";
  link.text = "SUPERCHARGER CSV DOWNLOAD";
  link.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(content.join("\n"))
  );
  link.setAttribute("download", "supercharger.csv");
  document.body.appendChild(link);
};

const EARNINGS = {
  "preparation course": 175,
  "preparation course (branded)": 175,
  "manager preparation course (branded)": 175,
  "behavioral (mentorship)": 150,
  "ios (mentorship)": 100,
  ios: 80,
  standard: 80,
  "eng management": 150,
  "systems design": 100,
  "systems design (mentorship)": 100,
  "standard (mentorship)": 115,
  behavioral: 100,
  frontend: 60,
  "frontend (mentorship)": 80,
};

const FAQ = `
How much do I get paid?
$60 / generic front-end interview or mentorship session
$60 / generic algorithmic interview or mentorship session
$100 / generic systems design interview or mentorship session
$100 / generic product design interview or mentorship session
$115 / company-themed algorithmic interview or mentorship session
$115 / generic behavioral interview or mentorship session
$115 / company-themed front-end interview or mentorship session
$150 / generic data eng interview or mentorship session
$150 / company-themed behavioral interview
$150 / company-themed systems design interview or mentorship session
$150 / company-themed product design interview or mentorship session
$150 / generic machine learning interview or mentorship session
$175 / generic iOS interview or mentorship session
$175 / generic Android systems design interview or mentorship session
$175 / generic eng manager interview or mentorship sessions
$200 / company-specific iOS interview or mentorship session
$200 / company-specific Android systems design interview or mentorship session
$200 / company-specific eng manager interview or mentorship session
$175 / company-specific machine learning interview or mentorship session
$175 / customized prep course session interview (non-branded)
$200 / personalized 1:1 branded session prep course
$275 / customized eng management prep course session interview (non-branded)
$300 / personalized 1:1 branded eng management session prep course

How and when will I be paid?
All interviews* are paid retroactively for the month that was two prior (net 60). For example, March interviews will be paid in full before May 30th. Payments are issued via direct deposit, for those with U.S. bank accounts via Gusto.

https://docs.google.com/document/d/1iiKLMjmW63qRtP6LfhqlIS4l_0ge7O8XY-nu92h6_-4/edit#heading=h.9yy5w1qsc8eh
`;

let allInterviews = [];
let interviewsByMonth = {};

async function fetchInterviews(after) {
  const token = getCookie("token");

  return fetch(
    `https://start.interviewing.io/api/interviews?needsReview=false${
      after ? `&after=${after}` : ""
    }`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  )
    .then((data) => data.json())
    .then((data) => {
      const { interviews, nextAfter } = data;
      allInterviews = [...allInterviews, ...interviews];
      interviewsByMonth = organizeInterviews(allInterviews);
      if (nextAfter) {
        return fetchInterviews(nextAfter);
      }
      return interviews;
    });
}

const organizeInterviews = (interviews) => {
  const months = {};
  interviews.forEach(({ round, createdTime, interviewee }) => {
    const momentObject = moment.utc(createdTime).local();
    const date = momentObject.format("YYYY-MM-DD");
    const key = momentObject.format("YYYY-MM");
    const focus = round.focus;
    const earning = EARNINGS[focus] || 0;

    const { review } = interviewee || {};
    const shared = {
      date,
      focus,
      earning,
      review,
      candidate: interviewee.pseudonym,
    };

    if (months[key]) {
      months[key] = [...months[key], { ...shared }];
    } else {
      months[key] = [{ ...shared }];
    }
  });

  return months;
};

const createTableFromInterviews = (allInterviews) => {
  const div = document.createElement("div");
  div.setAttribute("id", "page-extender-text-area-report");
  const table = document.createElement("table");
  const tbody = document.createElement("tbody");
  for (const dateKey in allInterviews) {
    const interviews = allInterviews[dateKey];
    const total = interviews
      .map((interview) => interview.earning)
      .reduce((prev, curr) => prev + curr, 0);

    const headerTR = document.createElement("tr");
    const headerTD = document.createElement("td");
    headerTD.innerText = `${dateKey} ($${total})`;
    headerTD.setAttribute("class", "header-row");
    headerTD.setAttribute("colspan", 9);
    headerTR.appendChild(headerTD);
    tbody.appendChild(headerTR);

    const columnNames = document.createElement("tr");
    [
      "date",
      "focus",
      "earning",
      "candidate",
      "workWithThem",
      "workWithThemExcited",
      "questionQuality",
      "questionHintQuality",
      "allowRematch",
    ].forEach((col) => {
      const td = document.createElement("td");
      td.innerText = col;
      columnNames.appendChild(td);
    });

    tbody.appendChild(columnNames);

    interviews.forEach((interview) => {
      const tr = document.createElement("tr");
      console.log(interview);
      const { date, focus, earning, review, candidate } = interview;
      const {
        workWithThem,
        workWithThemExcited,
        questionQuality,
        questionHintQuality,
        allowRematch,
      } = review || {};

      [
        date,
        focus,
        earning,
        candidate,
        workWithThem,
        workWithThemExcited,
        questionQuality,
        questionHintQuality,
        allowRematch,
      ].forEach((item) => {
        const td = document.createElement("td");
        td.innerText = item;
        tr.appendChild(td);
      });

      tbody.appendChild(tr);
    });
  }

  table.appendChild(tbody);
  div.appendChild(table);
  return div;
};

const createTable = () => {
  fetchInterviews();

  // Sorry, Hack god
  setTimeout(() => {
    const div = createTableFromInterviews(interviewsByMonth);
    document.body.appendChild(div);
  }, 1000);
};

loadScript(
  "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
).then(() => {
  if (document.querySelector("#page-extender-floating-button")) {
    return;
  }

  const extenderFloatingBtn = document.createElement("button");
  extenderFloatingBtn.setAttribute("id", "page-extender-floating-button");
  extenderFloatingBtn.innerHTML = "🐲";
  extenderFloatingBtn.addEventListener("click", () => {
    if (window.location.href.indexOf("history") < 0) {
      return alert("This only works on the history page");
    }

    setTimeout(() => {
      createTable();
    }, 1000);
  });

  document.body.appendChild(extenderFloatingBtn);
});
