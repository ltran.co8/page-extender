const getFaves = () => {
    const listings = [...document.querySelectorAll('.si-listings-column')]
    return listings.map(el => {
        const price = el.querySelector('.si-listing__photo-price span').innerText;
        const addr = `${el.querySelector('.si-listing__title-main').innerText} ${el.querySelector('.si-listing__title-description').innerText}`
        const link = el.querySelector('.si-listing a');
        
        const sqft = el.querySelector('.si-listing__info div:last-child .si-listing__info-value').innerText;
        return `${link}\t${addr}\t${price}\t${sqft}`;
    }).join('\n');
}

const showFavesAddr = () => {
    const display = document.createElement('textarea');
    display.setAttribute('id', 'page-extender-text-area-report');
    display.value = getFaves();
    document.body.appendChild(display);
}

const injectButton = () => {
    const floatingDiv = document.createElement('div');
    floatingDiv.setAttribute('id', 'floating-button-group');
    document.body.appendChild(floatingDiv);

    const extenderFloatingBtn = document.createElement('a');
    extenderFloatingBtn.innerHTML = '❤️';
    extenderFloatingBtn.href = '/search/results/?favs=true';
    floatingDiv.appendChild(extenderFloatingBtn);

    const goldBtn = document.createElement('button');
    goldBtn.innerHTML = 'faves';
    goldBtn.addEventListener('click', showFavesAddr);
    floatingDiv.appendChild(goldBtn);
};

injectButton();
