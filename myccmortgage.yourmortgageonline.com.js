const LOANS = {
  lafayette: "3630",
  shasta: "2447",
  whitney: "0369",
  rainier: "9531",
  denali: "4515",
};

const ASSET_ID = {
  lafayette: 13,
  shasta: 14,
  whitney: 15,
  rainier: 16,
  denali: 27,
};

const EXPENSE_CAT_ID = {
  lafayette: 74,
  shasta: 59,
  whitney: 52,
  rainier: 90,
  denali: 112,
};

const getLoansMetadata = async () => {
  const url = `https://myccmortgage.yourmortgageonline.com/v1/login/init`;
  const { loans } = await fetch(url).then((data) => data.json());
  return loans;
};

const generateTransactionURL = (hashedLoanNumber) => {
  return `https://myccmortgage.yourmortgageonline.com/v1/transactions/${hashedLoanNumber}?rowsPerPage=30&targetData=history`;
};

const createTable = async (loanName) => {
  const loanLast4 = LOANS[loanName];
  const loansMetadata = await getLoansMetadata();
  const hashedLoanNumber = loansMetadata.find(
    ({ maskedLoanNumber }) => maskedLoanNumber === `XXXXXX${loanLast4}`
  )?.hashedLoanNumber;

  if (!hashedLoanNumber) {
    alert(`${loanName} not found`);
    return;
  }

  const url = generateTransactionURL(hashedLoanNumber);
  fetch(url)
    .then((data) => data.json())
    .then((history) => {
      const { transactions: wrapper } = history;
      const { transactions } = wrapper;

      const headers = [
        "transactionDate",
        "interestAmount",
        "transactionDescription",
        "principalAmount",
        "escrowAmount",
        "escrowBalance",
        "amountPaid",
        "principalBalance",
      ].map((header) => `<th>${header}</th>`);

      const reducedHeaders = [
        "transactionDate",
        "description",
        "interestAmount",
        "note",
        "asset",
        "expenseCategoryId",
      ].map((header) => `<th>${header}</th>`);

      const rowData = transactions
        .map((transaction) => {
          const { transactionDate, ...rest } = transaction;
          return {
            ...rest,
            transactionDate: moment(transactionDate, "MM/DD/YYYY"),
          };
        })
        .sort((a, b) => b.transactionDate - a.transactionDate);

      const reducedRows = rowData.map((transaction) => {
        const { interestAmount, transactionDate } = transaction;

        const columns = [
          transactionDate.format("YYYY-MM-DD"),
          "CROSSCOUNTRY MORTGAGE, LLC",
          interestAmount,
          "",
          ASSET_ID[loanName],
          EXPENSE_CAT_ID[loanName],
        ].map((col) => `<td>${col}</td>`);
        return `<tr>${columns.join("")}</tr>`;
      });

      const rows = rowData.map((transaction) => {
        const {
          escrowAmount,
          amountPaid,
          interestAmount,
          principalAmount,
          escrowBalance,
          principalBalance,
          transactionDate,
          transactionDescription,
        } = transaction;

        const columns = [
          transactionDate.format("YYYY-MM-DD"),
          interestAmount,
          transactionDescription,
          principalAmount,
          escrowAmount,
          escrowBalance,
          amountPaid,
          principalBalance,
        ].map((col) => `<td>${col}</td>`);
        return `<tr>${columns.join("")}</tr>`;
      });

      const table = `
        <div>
            <table>
                <thead>${reducedHeaders}</thead>
                <tbody>${reducedRows}</tbody>
            </table>

            <table>
                <thead>${headers}</thead>
                <tbody>${rows}</tbody>
            </table>
        </div>
        `;

      const display = document.createElement("div");
      display.innerHTML = table;
      display.setAttribute("id", "page-extender-text-area-report");
      document.body.appendChild(display);
    })
    .catch((err) => alert(err));
};

const injectButton = () => {
  const floatingDiv = document.createElement("div");
  floatingDiv.setAttribute("id", "floating-button-group");
  document.body.appendChild(floatingDiv);

  const makenNewButton = (name) => {
    const extenderFloatingBtn = document.createElement("button");
    extenderFloatingBtn.innerHTML = name;
    extenderFloatingBtn.addEventListener("click", () => createTable(name));
    floatingDiv.appendChild(extenderFloatingBtn);
  };

  makenNewButton("lafayette");
  makenNewButton("shasta");
  makenNewButton("whitney");
  makenNewButton("rainier");
  makenNewButton("denali");
};

async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

loadScript(
  "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
).then(() => {
  injectButton();
});
