import pytesseract
import re
from PIL import Image
from enum import Enum
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from datetime import datetime
from pathlib import Path
import os

class ReceiptType(Enum):
    SAFEWAY = r'safeway'
    COSTCO = r'(costco|member)'
    TARGET = r'target|19661'
    SMART_FINAL = r'[se]m[ae]r.*final'


def _name_for_type(t):
    if t == ReceiptType.SMART_FINAL:
        return 'smart_final'
    if t == ReceiptType.COSTCO:
        return 'costco'
    if t == ReceiptType.TARGET:
        return 'target'
    return t.value


def _attempt_to_clean(input_string):
    replace_dict = {
        'MIL': "MILK",
        'MI': "Milk",
        'LUC': 'Lucerne Frozen Dairy Dessert Neapolitan 1 Gallon - 3.78 Liter',
        'FRIEND': '',
        'AC': '',
        ",": '.'
    }
    for word, replacement in replace_dict.items():
        input_string = re.sub(r'\b{}\b'.format(word), replacement, input_string, flags=re.IGNORECASE)
    return re.sub(r" ([^ ]*)$", "", input_string)


def _parse_receipt(t, txt):
    items = []
    date = None
    amount = 0
    try:
        if t == ReceiptType.SAFEWAY:
            if date := re.search(r"\d{2}/\d{2}/\d{2} \d{2}:\d{2}", txt):
                date = date.group()
                date = datetime.strptime(date, "%m/%d/%y %H:%M")
            if amount := re.search(r"PAYMENT AMOUNT\s*(\d+[\.,]\d{2})", txt):
                amount = amount.group(1).replace(',', '.')
            if items := re.findall(r".*\d+[\.,]\d{2}.*", txt):
                IGNORED = ('BALANCE', 'PAYMENT AMOUNT', 'CHANGE', 'Visa', 'TAX')
                items = [_attempt_to_clean(i) for i in items if all([x not in i for x in IGNORED])]
        elif t == ReceiptType.COSTCO:
            if date := re.search(r"\d{2}/\d{2}/\d{4} \d{2}:\d{2}", txt):
                date = date.group()
                date = datetime.strptime(date, "%m/%d/%Y %H:%M")
            if amount := re.search(r"Debit\s*(\d+[\.,]\d{2})", txt):
                amount = amount.group(1).replace(',', '.')
            if items := re.findall(r".*\d+[\.,]\d{2}.*", txt):
                IGNORED = ('Regular', 'Total Sale', 'EFT', 'Deb', 'SUBTOTAL', 'TAX', 'AMOUNT', 'CHANGE', 'INSTANT SAVINGS')
                items = [_attempt_to_clean(i) for i in items if all([x not in i for x in IGNORED])]
        elif t == ReceiptType.SMART_FINAL:
            if date := re.search(r"\d{2}/\d{2}/\d{2} \d{2}:\d{2} [AaPp]", txt):
                date = date.group()
                date = re.sub(r'[Aa]', 'AM', date)
                date = re.sub(r'[Pp]', 'PM', date)
                date = datetime.strptime(date, "%m/%d/%y %I:%M %p")
            if amount := re.search(r"Debit\s*(\d+[\.,]\d{2})", txt):
                amount = amount.group(1).replace(',', '.')
            else:
                if amount := re.search(r"PURCHASE.*\$(\d+[\.,]\d{2})", txt):
                    amount = amount.group(1).replace(',', '.')
            if items := re.findall(r".*\d+[\.,]\d{2}.*", txt):
                IGNORED = ('Debit', 'PURCHASE', 'SUBTOTAL', 'TOTAL', '=', '@', 'Regular Price', 'Total Saved', 'Item Savings')
                items = [_attempt_to_clean(i) for i in items if all([x not in i for x in IGNORED])]
        elif t == ReceiptType.TARGET:
            if date := re.search(r"\d{2}/\d{2}/\d{4} \d{2}:\d{2} [AaPp4F]", txt):
                date = date.group()
                date = re.sub(r'[Aa4]', 'AM', date)
                date = re.sub(r'[PpF]', 'PM', date)
                date = datetime.strptime(date, "%m/%d/%Y %I:%M %p")
            if amount := re.search(r"T?OTAL\s*\$(\d+[\.,]\d{2})", txt):
                amount = amount.group(1).replace(',', '.')
            if items := re.findall(r".*\$\d+[\.,]\d{2}.*", txt):
                IGNORED = ('TOTAL', 'TOTA', 'TAL', '@', '=', 'TAX', 'VISA')
                items = [_attempt_to_clean(i) for i in items if all([x not in i for x in IGNORED])]
    except Exception as e:
        pass
    n = _name_for_type(t)
    d_suffix = date.strftime("%m%d%y_%H%M") if isinstance(date, datetime) else ''
    return (
        date.strftime("%Y-%m-%d") if isinstance(date, datetime) else 'invalid_date', 
        n.upper(), 
        amount, 
        items, 
        f'{n}_{d_suffix}'
    )


def _parse_text(text):
    # print(text)
    # print('\n')
    for t in ReceiptType:
        if re.search(t.value, text, flags=re.IGNORECASE):
            return _parse_receipt(t, text)
    raise Exception("Unknown receipt type")


credentials_path = 'credentials.json'
SCOPES = ['https://www.googleapis.com/auth/drive.file']

def authenticate(credentials_path):
    creds = None
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(credentials_path, SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.json', 'w') as token:
            token.write(creds.to_json())
    return creds

def upload_file(service, file_path, file_name=None, folder_id=None):
    file_name = os.path.basename(file_path) if not file_name else file_name
    media = MediaFileUpload(file_path, resumable=True)
    file_metadata = {'name': file_name}
    if folder_id:
        file_metadata['parents'] = [folder_id]
    request = service.files().create(media_body=media, body=file_metadata)
    response = None
    while response is None:
        status, response = request.next_chunk()
        if status:
            print(f"Uploaded {int(status.progress() * 100)}%")
    print(f"File '{file_name}' uploaded successfully!")
    
    # Set permissions to make the file shareable
    permission = {
        'type': 'anyone',
        'role': 'reader'
    }
    service.permissions().create(fileId=response['id'], body=permission).execute()
    
    # Get the sharable link
    file_info = service.files().get(fileId=response['id'], fields='webViewLink').execute()
    sharable_link = file_info['webViewLink']
    
    print("Sharable Link:", sharable_link)
    return file_name, sharable_link

def _process_file(service, file_path):
    img = Image.open(file_path)
    text = pytesseract.image_to_string(img)
    try:
        date, description, amount, items, fn = _parse_text(text)
        folder_id = '1K7d3eC5mBYLvWuIX-rviUgIcHGPjR0Gb' # 2023 receipts
        _, link = upload_file(service, file_path, fn, folder_id)
        # link = 'ff'
        items = [f"- {i}" for i in items]
        items.append(f"- [Receipt]({link})")
        items = "\\n".join(items)
        return (date, description, amount, items, '33', '141')
    except Exception as e:
        print(f"Hit {e} while parsing {file_path=}")
        print(text)
        print('---------------\n\n')
        raise e
    


if __name__ == '__main__':
    creds = authenticate(credentials_path)
    service = build('drive', 'v3', credentials=creds)

    rows = []
    for root, dirs, files in os.walk(Path('/Users/longtra/Downloads/test')):
        for file in files:
            if file.lower().endswith(('.jpeg', '.jpg', '.png')):
                try:
                    r = _process_file(service, os.path.join(root, file))
                    print(r)
                    print('\n')
                    rows.append(','.join(r))
                except Exception as e:
                    pass
                    # print(f"Hit {e}. Skipping {file}")
                    
    with open('t.csv', 'w') as f:
        f.write('\n'.join(rows))