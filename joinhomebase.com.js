async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

async function fetchHoursSummary(start, end) {
  const url = `https://app.joinhomebase.com/timesheets/table_rows?end_date=${end}&start_date=${start}`;
  const response = await fetch(encodeURI(url));
  const json = await response.json();
  const { employees, rows } = json;
  const empMap = employees.reduce(
    (prev, curr) => ({ ...prev, [curr.id]: { ...curr } }),
    {}
  );
  const result = {};
  rows.forEach(({ job_id, actual_end_at, actual_start_at, date }) => {
    const start = moment(actual_start_at, "MM/DD/YYYY hh:mm");
    const end = moment(actual_end_at, "MM/DD/YYYY hh:mm");
    const duration = end - start;
    const name = empMap[job_id].full_name;
    if (!result[date]) {
      result[date] = {};
    }
    if (!isNaN(duration)) {
      result[date][name] = (result[date][name] || 0) + duration;
    }
  });
  const allocations = {};
  Object.entries(result).forEach(([date, labor]) => {
    const totalHours = Object.values(labor).reduce((p, c) => p + c, 0);
    allocations[date] = {};
    Object.entries(labor).forEach(([emp, hoursWorked]) => {
      allocations[date][emp] = hoursWorked / totalHours;
    });
  });

  return allocations;
}

const createTable = (content) => {
  const display = document.createElement("textarea");
  display.setAttribute("id", "page-extender-text-area-report");
  display.value = content;
  document.body.appendChild(display);
};

loadScript(
  "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
).then(() => {
  const extenderFloatingBtn = document.createElement("button");
  extenderFloatingBtn.setAttribute("id", "page-extender-floating-button");
  extenderFloatingBtn.innerHTML = "Report";
  extenderFloatingBtn.addEventListener("click", () => {
    setTimeout(async () => {
      const end = moment();
      const start = moment(end);
      start.subtract(20, "days");
      const rows = [];
      const report = await fetchHoursSummary(
        start.format("MM/DD/YYYY"),
        end.format("MM/DD/YYYY")
      );
      createTable(JSON.stringify(report, null, 2));
    }, 1000);
  });

  document.body.appendChild(extenderFloatingBtn);
});

console.log(
  Object.entries(labor)
    .sort((a, b) => a[0].localeCompare(b[0]))
    .map(([date, emps]) => {
      const intraDay = Object.entries(emps).map(
        ([emp, pct]) =>
          `${emp.replace(" ", "")}\t${((tips[date] * pct) / 100).toFixed(2)}`
      );
      return [date, ...intraDay].join("\n");
    })
    .join("\n")
);
