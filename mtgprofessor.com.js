const buttonHandler = () => {
	document.querySelector('input[name="LoanAmt"]').value = "35500.74";
	document.querySelector('input[name="intrate"]').value = "6.25";
	document.querySelector('input[name="loanterm"]').value = "144";
	
	// Monthly extra payment from month 1 to month 144
	document.querySelector('input[name="AmtOfChg1"]').value = "48.95";
	document.querySelector('input[name="StartChgInPeriod1"]').value = "1";
};

const injectButton = () => {
    const extenderFloatingBtn = document.createElement('button');
    extenderFloatingBtn.setAttribute('id', 'page-extender-floating-button');
    extenderFloatingBtn.innerHTML = 'ZT';
    extenderFloatingBtn.addEventListener('click', buttonHandler);
    document.body.appendChild(extenderFloatingBtn);
};

injectButton();
