async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

const createCSV = (rows) => {
  const content = [...rows];
  const link = document.createElement("a");
  link.id = "download-csv";
  link.text = "SUPERCHARGER CSV DOWNLOAD";
  link.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(content.join("\n"))
  );
  link.setAttribute("download", "supercharger.csv");
  document.body.appendChild(link);
};

const BRIE_ID = 12;
const BRIE_SUPERCHARGING_CATEGORY_ID = 27;
const GOUDA_ID = 28;
const GOUDA_SUPERCHARGING_CATEGORY_ID = 126;
const PARMESAN_ID = 36;
const PARMESAN_SUPERCHARGING_CATEGORY_ID = 160;
const RICOTTA_ID = 37;
const RICOTTA_SUPERCHARGING_CATEGORY_ID = 167;
const BRIE_VIN = "5YJ3E1EA9JF016058";
const GOUDA_VIN = "7SAYGDEE0NF402204";
const PARMESAN_VIN = "7SAYGDEE4PF693129";
const RICOTTA_VIN = "5YJ3E1EA9JF062697";

const getCarIDColumns = (vin) => {
  if (vin === BRIE_VIN) {
    return `${BRIE_ID},${BRIE_SUPERCHARGING_CATEGORY_ID}`;
  } else if (vin === GOUDA_VIN) {
    return `${GOUDA_ID},${GOUDA_SUPERCHARGING_CATEGORY_ID}`;
  } else if (vin === PARMESAN_VIN) {
    return `${PARMESAN_ID},${PARMESAN_SUPERCHARGING_CATEGORY_ID}`;
  } else if (vin === RICOTTA_VIN) {
    return `${RICOTTA_ID},${RICOTTA_SUPERCHARGING_CATEGORY_ID}`;
  }
};

const createTable = () => {
  fetch("https://www.tesla.com/teslaaccount/charging/api/history?vin=")
    .then((data) => data.json())
    .then((history) => {
      const { data } = history;
      const content = data
        .sort(
          (a, b) =>
            moment(a.chargeStartDateTime) - moment(b.chargeStartDateTime)
        )
        .map(({ chargeStartDateTime, siteLocationName, fees, vin }) => {
          const charges = fees
            .filter(({ pricingType }) => pricingType === "PAYMENT")
            .map(({ totalDue }) => totalDue)
            .join(",");
          return `${moment(chargeStartDateTime).format(
            "YYYY-MM-DD hh:mm A"
          )},${moment(chargeStartDateTime).format(
            "YYYY-MM-DD"
          )},\"Supercharging - ${siteLocationName}\",${charges},,${getCarIDColumns(
            vin
          )}`;
        });

      createCSV(content);

      const display = document.createElement("textarea");
      display.setAttribute("id", "page-extender-text-area-report");
      display.value = content.join("\n");
      document.body.appendChild(display);

      console.log(history);
    })
    .catch((err) => alert(err));
};

loadScript(
  "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
).then(() => {
  const extenderFloatingBtn = document.createElement("button");
  extenderFloatingBtn.setAttribute("id", "page-extender-floating-button");
  extenderFloatingBtn.innerHTML = "Gouda";
  extenderFloatingBtn.addEventListener("click", () => {
    setTimeout(() => {
      createTable();
    }, 1000);
  });

  document.body.appendChild(extenderFloatingBtn);
});
