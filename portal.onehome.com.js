const extenderFloatingBtn = document.createElement('button');
extenderFloatingBtn.setAttribute('id', 'page-extender-floating-button');
extenderFloatingBtn.innerHTML = '♥️';
extenderFloatingBtn.addEventListener('click', () => {
    if (window.location.href.indexOf('properties/list') < 0) {
        return alert('This only works in the list page');
    }

    let tiles = [...document.querySelectorAll('.tile-container .item')];
    tiles = [...tiles, ...document.querySelectorAll('.card-wrapper')];
    
    for (const tile of tiles) {
        if (tile.querySelector('.dislike-dropdown svg > g').getAttribute('fill')) {
            tile.remove();
        }
    }
});

document.body.appendChild(extenderFloatingBtn);