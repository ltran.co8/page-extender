const buttonHandler = () => {
    document.querySelector('select[data-test="filter-column-0"]').value = "name";
    document.querySelector('select[data-test="filter-op-0"]').value = "$ilike";
    const q = document.querySelector('input[data-test="filter-value-0"]');
    q.value = "%%";
    q.focus();
};

const injectButton = () => {
    const extenderFloatingBtn = document.createElement('button');
    extenderFloatingBtn.setAttribute('id', 'page-extender-floating-button');
    extenderFloatingBtn.innerHTML = 'Name';
    extenderFloatingBtn.addEventListener('click', buttonHandler);
    document.body.appendChild(extenderFloatingBtn);
};

injectButton();