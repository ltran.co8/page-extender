const ASSET_ID = {
  mitchell: 4,
  lafayette: 13,
  shasta: 14,
  whitney: 15,
  rainier: 16,
  tai: 18,
  denali: 27,
  fuji: 34,
};

const MGT_FEES_CAT_ID = {
  mitchell: 70,
  lafayette: 69,
  shasta: 56,
  whitney: 46,
  rainier: 91,
  tai: 109,
  denali: 110,
  fuji: 154,
};

const MAINTENANCE_CAT_ID = {
  mitchell: 82,
  lafayette: 81,
  shasta: 63,
  whitney: 51,
  rainier: 83,
  tai: 113,
  denali: 114,
  fuji: 152,
};

const WATER_CAT_ID = {
  mitchell: 86,
  lafayette: 73,
  shasta: 58,
  whitney: 49,
  rainier: 127,
  tai: 129,
  denali: 131,
  fuji: 149,
};

const TRASH_CAT_ID = {
  mitchell: 102,
  lafayette: 103,
  shasta: 104,
  whitney: -1,
  rainier: 128,
  tai: 132,
  denali: 133,
  fuji: 148,
};

const BUILDIUM_BUILDING_ID = {
  mitchell: 212014,
  lafayette: 212155,
  shasta: 212157,
  whitney: 193619,
  rainier: 207956,
  tai: 223865,
  denali: 226118,
  fuji: 253634,
};

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

const makeHTMLTable = (headers, rows) => {
  return `<table>
                <thead>${headers}</thead>
                <tbody>${rows}</tbody>
            </table>`;
};

const SHARED_PAYLOAD = {
  PropertySelectionType: "Rental",
  // StartDate: `${new Date().getFullYear()}-01-01`,
  // EndDate: `${new Date().getFullYear()}-12-31`,
  StartDate: `2022-01-01`,
  EndDate: `2022-12-31`,
  AccountingBasis: 1,
};

const buildiumPayloadForMgtFee = (propName) => {
  return {
    ...SHARED_PAYLOAD,
    PropertySelectionEntityId: BUILDIUM_BUILDING_ID[propName],
    GlAccountIds: ["124373"],
  };
};

const buildiumPayloadForRepairs = (propName) => {
  return {
    ...SHARED_PAYLOAD,
    PropertySelectionEntityId: BUILDIUM_BUILDING_ID[propName],
    GlAccountIds: ["124378"],
  };
};

const buildiumPayloadForWater = (propName) => {
  return {
    ...SHARED_PAYLOAD,
    PropertySelectionEntityId: BUILDIUM_BUILDING_ID[propName],
    GlAccountIds: ["124381"],
  };
};

const buildiumPayloadForTrash = (propName) => {
  return {
    ...SHARED_PAYLOAD,
    PropertySelectionEntityId: BUILDIUM_BUILDING_ID[propName],
    GlAccountIds: ["371815"],
  };
};

const fetchBuildiumData = async (propName, payload) => {
  let data = await fetch(
    `https://rentingdoors.managebuilding.com/Manager/api/generalLedger/transactions`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-XSRF-TOKEN": getCookie("XSRF-TOKEN"),
      },
      body: JSON.stringify(payload),
    }
  );
  data = await data.json();
  if (data?.length <= 0) {
    return [];
  }
  const { Transactions } = data[0];
  const transactions = Transactions;
  console.log(transactions);
  return transactions.map((t) => {
    const { Date, Amount, Name, Description } = t;
    return { date: Date, amount: Amount, description: Name, note: Description };
  });
};

const createBuildiumTransactionRows = async (
  propName,
  payload,
  expenseCategoryDict
) => {
  const rawData = await fetchBuildiumData(propName, payload);
  const rowData = rawData
    .map((transaction) => {
      const { date, ...rest } = transaction;
      return {
        ...rest,
        date: moment(date),
      };
    })
    .sort((a, b) => new Date(b.date) - new Date(a.date));

  return rowData.map((transaction) => {
    const { date, amount, description, note } = transaction;

    const columns = [
      date.format("YYYY-MM-DD"),
      description,
      amount,
      note,
      ASSET_ID[propName],
      expenseCategoryDict[propName],
      propName,
    ].map((col) => `<td>${col}</td>`);
    return `<tr>${columns.join("")}</tr>`;
  });
};

const createMgtFeesRows = async (propName) => {
  return createBuildiumTransactionRows(
    propName,
    buildiumPayloadForMgtFee(propName),
    MGT_FEES_CAT_ID
  );
};

const createRepairRows = async (propName) => {
  return createBuildiumTransactionRows(
    propName,
    buildiumPayloadForRepairs(propName),
    MAINTENANCE_CAT_ID
  );
};

const createWaterRows = async (propName) => {
  return createBuildiumTransactionRows(
    propName,
    buildiumPayloadForWater(propName),
    WATER_CAT_ID
  );
};

const createTrashRows = async (propName) => {
  return createBuildiumTransactionRows(
    propName,
    buildiumPayloadForTrash(propName),
    TRASH_CAT_ID
  );
};

const createBuidiumTable = async (rowGenFunc) => {
  const props = [
    "mitchell",
    "lafayette",
    "shasta",
    "whitney",
    "rainier",
    "tai",
    "denali",
    "fuji",
  ];

  let allRows = [];
  let i = 0;
  while (i < props.length) {
    const rows = await rowGenFunc(props[i++]);
    allRows = allRows.concat(rows);
  }

  const reducedHeaders = [
    "date",
    "description",
    "amount",
    "note",
    "asset",
    "expenseCatId",
    "name",
  ]
    .map((header) => `<th>${header}</th>`)
    .join("");

  return makeHTMLTable(reducedHeaders, allRows.join(""));
};

const createTable = async (propName) => {
  const display = document.createElement("div");
  display.innerHTML = `
    <h1 style="background: orange">MGT FEES</h1>
    ${await createBuidiumTable(createMgtFeesRows)}

    <h1 style="background: orange">REPAIRS</h1>
    ${await createBuidiumTable(createRepairRows)}

    <h1 style="background: orange">WATER</h1>
    ${await createBuidiumTable(createWaterRows)}

    <h1 style="background: orange">TRASH</h1>
    ${await createBuidiumTable(createTrashRows)}
    `;
  display.setAttribute("id", "page-extender-text-area-report");
  document.body.appendChild(display);
};

const injectButton = () => {
  const floatingDiv = document.createElement("div");
  floatingDiv.setAttribute("id", "floating-button-group");
  document.body.appendChild(floatingDiv);

  const _makenNewButton = (name, callback) => {
    const extenderFloatingBtn = document.createElement("button");
    extenderFloatingBtn.innerHTML = name;
    extenderFloatingBtn.addEventListener("click", callback);
    floatingDiv.appendChild(extenderFloatingBtn);
  };

  const makenNewButton = (name) => {
    _makenNewButton(name, () => createTable(name));
  };

  makenNewButton("GO");
  _makenNewButton("close", () =>
    document.removeChild(
      document.getElementById("page-extender-text-area-report")
    )
  );
};

async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

loadScript(
  "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
).then(() => {
  injectButton();
});
