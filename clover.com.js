async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

async function fetchSalesReportSummary(start, end) {
  const params = [
    ["comparison", `SAME_PERIOD_LAST_WEEK`],
    ["filter", `createdTime<${end}`],
    ["filter", `createdTime>${start}`],
    ["expand", `cashSummary`],
    ["expand", `declinedOfflinePayments`],
    ["expand", `salesSummaries`],
    ["expand", `tendersSummary`],
    ["expand", `top5Items`],
    ["expand", `top5Categories`],
    ["expand", `top5RevenueClasses`],
    ["period", `HOUR`],
  ];
  const joined = params
    .map(([k, v]) => `${k}=${encodeURIComponent(v)}`)
    .join("&");
  const url = `https://www.clover.com/v3/merchants/9D0EMDP9Z9W11/reports/sales_grouped_compared?filter=${joined}`;

  const response = await fetch(url);
  const json = await response.json();
  console.log(json);
  return json;
}

const createTable = (content) => {
  const display = document.createElement("textarea");
  display.setAttribute("id", "page-extender-text-area-report");
  display.value = content;
  document.body.appendChild(display);
};

loadScript(
  "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
).then(() => {
  const extenderFloatingBtn = document.createElement("button");
  extenderFloatingBtn.setAttribute("id", "page-extender-floating-button");
  extenderFloatingBtn.innerHTML = "Tip";
  extenderFloatingBtn.addEventListener("click", () => {
    setTimeout(async () => {
      let now = moment();
      const stop = moment(now);
      stop.subtract(20, "days");
      const rows = {};
      while (now > stop) {
        const start = now.startOf("day").toDate().valueOf();
        const end = now.endOf("day").toDate().valueOf();
        const report = await fetchSalesReportSummary(start, end);
        console.log(report);
        const tip = report?.current?.salesSummaries?.total?.tipAmount;
        rows[now.format("MM/DD/YYYY")] = tip;
        now = now.subtract(1, "days");
      }
      createTable(JSON.stringify(rows, null, 2));
    }, 1000);
  });

  document.body.appendChild(extenderFloatingBtn);
});
