const buttonHandler = () => {
    const [start, end] = location.search.replace('?block=', '').split('---').map(c => decodeURIComponent(c));

    fetch('https://www.getaround.com/ajax/calendar_event/add', {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
        body: `carid=100008133031185&start_time=${start}&end_time=${end}&widget=monthly-calendar`
    })
        .then(data => {
            alert('Blocked off time!');
            location.reload();
        })
        .catch(err => {
            alert(err);
            err => console.log(err)
        });
};

const injectButton = () => {
    const extenderFloatingBtn = document.createElement('button');
    extenderFloatingBtn.setAttribute('id', 'page-extender-floating-button');
    extenderFloatingBtn.innerHTML = 'ZT';
    extenderFloatingBtn.addEventListener('click', buttonHandler);
    document.body.appendChild(extenderFloatingBtn);
};

injectButton();