async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

async function fetchPayrollHistory() {
  const url = `https://app.gusto.com/api/companies/c53a05f4-227a-44e4-97d3-cbde5f521c17/payrolls?page=1&page_size=10&include_externals=true`;
  const response = await fetch(url);
  const history = await response.json();
  console.log(history);

  const cleaned = [];
  history
    .sort((a, b) => a.debit_date?.localeCompare(b.debit_date))
    .forEach((h) => {
      const link = `https://app.gusto.com/payrolls/${h.id}`;
      const shared = {
        date: h.debit_date,
        description: "Gusto",
        amount: 0,
        note: null,
        asset: 33,
        category: 142,
      };
      const pay = { ...shared };
      pay.amount = parseFloat(h.total_net_pay);
      pay.note = `- [Payroll ${h.payment_period_start_at} → ${h.payment_period_end_at}](${link})`;
      const tax = { ...shared };
      tax.amount = parseFloat(h.total_payable_tax);
      tax.note = `- [Payroll ${h.payment_period_start_at} → ${h.payment_period_end_at} tax](${link})`;
      cleaned.push(pay);
      cleaned.push(tax);
    });
  console.table(cleaned);

  return cleaned;
}

function createTableFromJSON(jsonData) {
  // Create a table element
  var table = document.createElement("table");

  // Create a table header row
  var headers = Object.keys(jsonData[0]);
  var headerRow = document.createElement("tr");
  headers.forEach(function (headerText) {
    var headerCell = document.createElement("th");
    headerCell.textContent = headerText;
    headerRow.appendChild(headerCell);
  });
  table.appendChild(headerRow);

  // Create table rows and cells
  jsonData.forEach(function (rowData) {
    var row = document.createElement("tr");
    headers.forEach(function (headerText) {
      var cell = document.createElement("td");
      cell.textContent = rowData[headerText];
      row.appendChild(cell);
    });
    table.appendChild(row);
  });

  return table;
}

const createTable = (content) => {
  const display = createTableFromJSON(content);
  display.setAttribute("id", "page-extender-text-area-report");
  display.style = "font-size: 10px;";
  document.body.appendChild(display);
};

loadScript(
  "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
).then(() => {
  const extenderFloatingBtn = document.createElement("button");
  extenderFloatingBtn.setAttribute("id", "page-extender-floating-button");
  extenderFloatingBtn.innerHTML = "Report";
  extenderFloatingBtn.addEventListener("click", () => {
    setTimeout(async () => {
      const report = await fetchPayrollHistory();
      createTable(report);
    }, 100);
  });

  document.body.appendChild(extenderFloatingBtn);
});
