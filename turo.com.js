const momentizeDate = (date, time) => {
  return moment(`${date}${time}`, "YYYY-MM-DDhh:mm");
  // return moment(`${date} ${moment().year()} ${time}`, "ddd, MMM D YYYY h:mm A");
};

const createDate = (date, time) => {
  const m = momentizeDate(date, time);
  const FMT = "YYYYMMDDTHHmm00";
  return [m.format(FMT), m.add(30, "minutes").format(FMT)];
};

const download = (fn, data) => {
  const blob = new Blob([data], { type: "text/plain" }),
    e = document.createEvent("MouseEvents"),
    a = document.createElement("a");

  a.download = fn;
  a.href = window.URL.createObjectURL(blob);
  a.dataset.downloadurl = ["text/plain", a.download, a.href].join(":");
  e.initMouseEvent(
    "click",
    true,
    false,
    window,
    0,
    0,
    0,
    0,
    0,
    false,
    false,
    false,
    false,
    0,
    null
  );
  a.dispatchEvent(e);
};

const createICS = (title, name, phone, addr, start, end) => {
  return `BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
BEGIN:VTIMEZONE
TZID:America/Los_Angeles
TZURL:http://tzurl.org/zoneinfo-outlook/America/Los_Angeles
X-LIC-LOCATION:America/Los_Angeles
BEGIN:DAYLIGHT
TZOFFSETFROM:-0800
TZOFFSETTO:-0700
TZNAME:PDT
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:-0700
TZOFFSETTO:-0800
TZNAME:PST
RRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU
END:STANDARD
END:VTIMEZONE
BEGIN:VEVENT
DTSTART;TZID=America/Los_Angeles:${start}
DTEND;TZID=America/Los_Angeles:${end}
SUMMARY:${title}
URL:${encodeURI(window.location.href)}
DESCRIPTION:${name}: ${phone}
LOCATION:${addr}
END:VEVENT
END:VCALENDAR`;
};

async function loadScript(url) {
  let response = await fetch(url);
  let script = await response.text();
  eval(script);
}

const filterTeslaOnly = () => {
  console.log(document.querySelector(".dropdown-button--bold"));
  document.querySelectorAll(".dropdown-button--bold")[0].click();
  setTimeout(() => {
    //[...document.querySelectorAll('.dropdownVehicleItem')].pop().click();
  }, 500);
};

const CAR_NAMES = {
  "5YJ3E1EA9JF016058": "Brie",
  "7SAYGDEE0NF402204": "Gouda",
  "7SAYGDEE4PF693129": "Parmesan",
  "5YJ3E1EA9JF062697": "Ricotta",
};

const INCOME_SOURCE_ID_MAPPING = {
  Brie: 14,
  Cheddar: 15,
  Gouda: 30,
  Parmesan: 33,
  Ricotta: 45,
};

const fetchTripInfo = async (tripID) => {
  return fetch(
    `https://turo.com/api/reservation/detail?oppTermsAware=true&reservationId=${tripID}`
  )
    .then((response) => response.json())
    .then((response) => {
      const { id, location, vehicle, booking, renter } = response;
      const { address } = location;
      const { vin } = vehicle;
      const { name: renterName, mobilePhone } = renter;
      const { cost, start, end } = booking;

      console.log(response);

      return {
        id,
        address:
          address.indexOf("Tantau") >= 0
            ? null
            : address.indexOf("San Jose") >= 0
            ? "1701 Airport Blvd\\, San Jose\\, CA  95110"
            : "San Francisco International Airport\\, San Francisco",
        cost,
        carName: CAR_NAMES[vin],
        renterName,
        startDate: start.localDate,
        startTime: start.localTime,
        endDate: end.localDate,
        endTime: end.localTime,
        phone: mobilePhone?.e164,
      };
    });
};

const downloadICS = async () => {
  if (window.location.href.indexOf("booked") >= 0) {
    filterTeslaOnly();
    return;
  }

  if (window.location.href.indexOf("/reservation/") < 0) {
    return alert("This only works on the reservation page");
  }

  const tripID = window.location.href.match(/.*reservation\/([0-9]*)\/?.*/)[1];
  const x = await fetchTripInfo(tripID);

  const {
    address: deliveryAddress,
    startDate,
    startTime,
    endDate,
    endTime,
    phone,
    carName,
    renterName: name,
  } = x;

  console.log(x);

  loadScript(
    "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
  )
    .then(() => {
      const [tripStart, x] = createDate(startDate, startTime);
      const [tripEnd, y] = createDate(endDate, endTime);
      download(
        `${name}-trip.ics`,
        createICS(
          `${name}'s Trip with ${carName}`,
          name,
          phone,
          "",
          tripStart,
          tripEnd
        )
      );

      return new Promise((resolve) => setTimeout(resolve, 1000));
    })
    .then(() => {
      if (deliveryAddress) {
        const [dropOffStart, dropOffEnd] = createDate(startDate, startTime);
        download(
          `${name}-dropoff.ics`,
          createICS(
            `Drop off ${carName}`,
            name,
            phone,
            deliveryAddress,
            dropOffStart,
            dropOffEnd
          )
        );
      }

      return new Promise((resolve) => setTimeout(resolve, 1000));
    })
    .then(() => {
      if (deliveryAddress) {
        const [pickUpStart, pickUpEnd] = createDate(endDate, endTime);
        download(
          `${name}-pickup.ics`,
          createICS(
            `Pick up ${carName}`,
            name,
            phone,
            deliveryAddress,
            pickUpStart,
            pickUpEnd
          )
        );
      }

      const OFFSET = 4;
      const tripStart = momentizeDate(startDate, startTime)
        .subtract(OFFSET, "hours")
        .format("YYYY-MM-DDTHH:mm:00");
      const tripEnd = momentizeDate(endDate, endTime)
        .add(OFFSET, "hours")
        .format("YYYY-MM-DDTHH:mm:00");
    })
    .catch((err) => {
      console.error(err);
    });
};

const reportIncome = () => {
  const tripID = window.location.href.match(/.*reservation\/([0-9]*)\/?.*/)[1];

  fetchTripInfo(tripID)
    .then((response) => {
      const { cost, endDate, id, carName, renterName } = response;
      console.log(response);
      const note = `[https://turo.com/reservation/${tripID}/receipt](https://turo.com/reservation/${tripID}/receipt)`;
      const url = encodeURI(
        `https://gold.ltran.co/quick-import/?date=${endDate}&amount=${cost}&source_id=${INCOME_SOURCE_ID_MAPPING[carName]}&note=${note}&description=${renterName}`
      );
      console.log(url);
      window.open(url);
    })
    .catch((error) => {
      console.log(error);
    });
};

const injectButton = () => {
  const floatingDiv = document.createElement("div");
  floatingDiv.setAttribute("id", "floating-button-group");
  document.body.appendChild(floatingDiv);

  const extenderFloatingBtn = document.createElement("button");
  extenderFloatingBtn.innerHTML = "ics";
  extenderFloatingBtn.addEventListener("click", downloadICS);
  floatingDiv.appendChild(extenderFloatingBtn);

  const goldBtn = document.createElement("button");
  goldBtn.innerHTML = "gold";
  goldBtn.addEventListener("click", reportIncome);
  floatingDiv.appendChild(goldBtn);
};

injectButton();
